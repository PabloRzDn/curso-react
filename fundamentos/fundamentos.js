import {fetch} from "cross-fetch";

//******Funciones********

//funcion normal

function sumar_dos(num1,num2){
    const sumando=num1+num2
    console.log(sumando)
}
//sumar_dos(2,2)

//funcion flecha

const sumarDos=(num_1,num_2)=>{
    const suma=num_1+num_2
    console.log(suma)
}

//sumarDos(2,3)


//********template String*********/

const esPar=(num)=>{
    if(num%2==0){
        return ` el número ${num} es par`
    } else{
        return ` el numero ${num} es impar`
    }
    
}

const resPar=esPar(2.0)
const resImpar=esPar(3.0)

//console.log(resPar)
//console.log(resImpar)

//****OBJETOS*******/



const usuario={
    id:1,
    nombre:"Pablo",
    apellido:"Ruz",
    edad:30,
    deportes:["futbol","ciclismo"],
    trabaja:true
}

/*console.log(usuario)
console.log(usuario.trabaja)
console.log(usuario.nombre)
console.log(usuario.deportes)
*/

//FETCH API

/*
fetch("https://pokeapi.co/api/v2/pokemon/")
.then(
    (res)=>res.json()
)
.then(
    data=>{
        console.log(data.results)
        data.results.forEach(elem => {
            console.log(elem.name)
        })
    })
    .catch(e=>console.log(e))
*/
    //ASYNC AWAIT


const obtenerPokemones=async()=>{
    try{
        const res=await fetch("https://pokeapi.co/api/v2/pokemon/")
        const data=await res.json()
        
        const arregloNombres=data.results.map(pokemon => pokemon.name)
        console.log(arregloNombres)
    }catch(e){
        console.log(e)
    }

}

obtenerPokemones()
